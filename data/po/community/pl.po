# 
# Translators:
# Marcin Sedlak <fdmarcin@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-04-03 14:00+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Polish (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/pl/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pl\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: community.html:17
msgid "Community"
msgstr "Społeczność"

#: community.html:25
msgid "Forum"
msgstr "Forum"

#: community.html:26
msgid ""
"To play an active role and help shape the direction of Ubuntu MATE, the "
"forum is the best place to go. From here, you can talk with the Ubuntu MATE "
"team and other members of our growing community."
msgstr ""

#: community.html:30
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "Wygląda na to, że nie masz aktywnego połączenia z internetem. Sprawdź swoje połączenie."

#: community.html:31
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "Przepraszamy, program Welcome nie mógł uzyskać połączenia."

#: community.html:32
msgid "Retry"
msgstr "Spróbuj ponownie"

#: community.html:37
msgid "Social Networks"
msgstr "Sieci społecznościowe"

#: community.html:38
msgid "Ubuntu MATE is active on the following social networks."
msgstr "Ubuntu MATE jest aktywne w następujących sieciach społecznościowych."
