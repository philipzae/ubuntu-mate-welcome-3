# 
# Translators:
# Mubarak Qahtani <abu-q76@hotmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-06-19 04:21+0000\n"
"Last-Translator: Mubarak Qahtani <abu-q76@hotmail.com>\n"
"Language-Team: Arabic (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/ar/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: ar\n"
"Plural-Forms: nplurals=6; plural=n==0 ? 0 : n==1 ? 1 : n==2 ? 2 : n%100>=3 && n%100<=10 ? 3 : n%100>=11 && n%100<=99 ? 4 : 5;\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: community.html:18
msgid "Community"
msgstr "مجتمع"

#: community.html:27
msgid "Forum"
msgstr "منتدى"

#: community.html:28
msgid ""
"To play an active role and help shape the direction of Ubuntu MATE, the "
"forum is the best place to go. From here, you can talk with the Ubuntu MATE "
"team and other members of our growing community."
msgstr "لتلعب دورًا هامًا وتساهم في تحسين \"أبونتو ماتيه\"، المنتدى هو أفضل مكان. من المنتدى يمكنك التواصل مع فريق \"أبونتو ماتيه\" وأيضا مع اعضاء المجتمع المتزايد."

#: community.html:32
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "من الواضح انك لست متصلًا بالانترنت. رجاءًا تأكد من اتصالك بالانترنت لكي ترى هذا المحتوى"

#: community.html:33
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "عفوًا، برنامج الترحيب ليس قادر على انشاء اتصال"

#: community.html:34
msgid "Retry"
msgstr "إعادة محاولة"

#: community.html:39
msgid "Social Networks"
msgstr "الشبكات اﻹجتماعية"

#: community.html:40
msgid "Ubuntu MATE is active on the following social networks."
msgstr "\"أبونتو ماتيه\" فعالة في هذه الشبكات الإجتماعية."
