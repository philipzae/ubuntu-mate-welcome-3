# 
# Translators:
# , 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-04-03 14:00+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Belarusian (Belarus) (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/be_BY/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: be_BY\n"
"Plural-Forms: nplurals=4; plural=(n%10==1 && n%100!=11 ? 0 : n%10>=2 && n%10<=4 && (n%100<12 || n%100>14) ? 1 : n%10==0 || (n%10>=5 && n%10<=9) || (n%100>=11 && n%100<=14)? 2 : 3);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: chatroom.html:17
msgid "Chat Room"
msgstr "Чат-пакой"

#: chatroom.html:25
msgid "Chat with fellow users"
msgstr "Чат з іншымі карыстальнікамі"

#: chatroom.html:26
msgid "This IRC client is pre-configured so you can jump into our"
msgstr "IRC-кліент ужо настаўлены, вы можаце адразу ўвайсці на наш канал"

#: chatroom.html:27
msgid "channel on"
msgstr "на"

#: chatroom.html:28
msgid ""
"Most of the Ubuntu MATE team are on here but they have real lives too. If "
"you have a question, feel free to ask."
msgstr ""

#: chatroom.html:30
msgid ""
"However, it may take a while for someone to reply. Just be patient and don't"
" disconnect right away."
msgstr "Аднак чаканне адказу можа быць доўгім. Проста будзьце цярплівымі і не адключайцеся занадта хутка."

#: chatroom.html:34
msgid ""
"It appears you are not connected to the Internet. Please check your "
"connection to access this content."
msgstr "Выглядае, што вы не злучаны з інтэрнэтам. Праверце ваша злучэнне, каб атрымаць доступ да змесціва."

#: chatroom.html:35
msgid "Sorry, Welcome was unable to establish a connection."
msgstr "Выбачайце, Welcome не здолеў усталяваць злучэнне."

#: chatroom.html:36
msgid "Retry"
msgstr "Паўтарыць"

#: chatroom.html:39
msgid "Join the IRC in Hexchat"
msgstr "Увайсці ў IRC праз Hexchat"
