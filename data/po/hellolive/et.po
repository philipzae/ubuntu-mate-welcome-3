# 
# Translators:
# Olari Pipenberg <olari.pipenberg@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"POT-Creation-Date: 2016-02-27 12:48+0100\n"
"PO-Revision-Date: 2016-05-28 22:42+0000\n"
"Last-Translator: Olari Pipenberg <olari.pipenberg@gmail.com>\n"
"Language-Team: Estonian (http://www.transifex.com/ubuntu-mate/ubuntu-mate-welcome/language/et/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: et\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"Report-Msgid-Bugs-To : you@example.com\n"

#: hellolive.html14, 28
msgid "Welcome"
msgstr "Tere tulemast"

#: hellolive.html:24
msgid "Hello."
msgstr "Tere!"

#: hellolive.html:25
msgid "Thank you for downloading Ubuntu MATE and joining our community."
msgstr "Tänan, et laadisite alla Ubuntu MATE ja liitusite meie kogukonnaga."

#: hellolive.html:27
msgid "The"
msgstr "See"

#: hellolive.html:28
msgid ""
"application provides useful documentation on getting started with your "
"computer. Once installed, Welcome will showcase a large array of "
"applications that will get the most out of your computing."
msgstr "rakendus annab kasulikku dokumentatsiooni alustamisest. Welcome pakub suurt valikut erinevaid rakendusi, et pakkuda head kasutuskogemust."

#: hellolive.html:32
msgid "We hope you enjoy Ubuntu MATE."
msgstr "Me loodame, et Teile meeldib Ubuntu MATE"

#: hellolive.html:34
msgid "Continue"
msgstr "Jätka"
