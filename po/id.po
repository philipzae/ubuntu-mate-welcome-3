# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
#
# Translators:
# Guz Firdaus <guzfirdaus@gmail.com>, 2016
msgid ""
msgstr ""
"Project-Id-Version: Ubuntu MATE Welcome\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2016-06-14 18:20+0100\n"
"PO-Revision-Date: 2016-06-06 16:10+0000\n"
"Last-Translator: Martin Wimpress <code@flexion.org>\n"
"Language-Team: Indonesian (http://www.transifex.com/ubuntu-mate/ubuntu-mate-"
"welcome/language/id/)\n"
"Language: id\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"

#: ubuntu-mate-welcome:71
msgid ""
"Software changes are in progress. Please allow them to complete before "
"closing Welcome."
msgstr ""
"Perubahan terhadap aplikasi sedang berlangsung. Tunggu hingga selesai "
"sebelum menutup Selamat Datang."

#: ubuntu-mate-welcome:72 ubuntu-mate-welcome:2489
msgid "OK"
msgstr "OK"

#: ubuntu-mate-welcome:159 ubuntu-mate-welcome:169
msgid "Successfully performed fix."
msgstr ""

#: ubuntu-mate-welcome:159
msgid "Any previously incomplete installations have been finished."
msgstr ""

#: ubuntu-mate-welcome:162 ubuntu-mate-welcome:172
msgid "Failed to perform fix."
msgstr ""

#: ubuntu-mate-welcome:162
msgid "Errors occurred while finishing an incomplete installation."
msgstr ""

#: ubuntu-mate-welcome:169
msgid "Packages with broken dependencies have been resolved."
msgstr ""

#: ubuntu-mate-welcome:172
msgid "Packages may still have broken dependencies."
msgstr ""

#: ubuntu-mate-welcome:181
msgid "Successfully updated cache."
msgstr ""

#: ubuntu-mate-welcome:181
msgid "Software is now ready to install."
msgstr ""

#: ubuntu-mate-welcome:184
msgid "Failed to update cache."
msgstr ""

#: ubuntu-mate-welcome:184
msgid "There may be a problem with your repository configuration."
msgstr ""

#: ubuntu-mate-welcome:240
msgid "Welcome will stay up-to-date."
msgstr ""

#: ubuntu-mate-welcome:241
#, fuzzy
msgid ""
"Welcome and the Software Boutique are set to receive the latest updates."
msgstr "Aplikasi ini disetel untuk menerima pembaruan terakhir."

#: ubuntu-mate-welcome:247
msgid "Everything is up-to-date"
msgstr ""

#: ubuntu-mate-welcome:248
msgid "All packages have been upgraded to the latest versions."
msgstr ""

#: ubuntu-mate-welcome:253
#, fuzzy
msgid "Installed"
msgstr "Pasang"

#: ubuntu-mate-welcome:254
#, fuzzy
msgid "The application is now ready to use."
msgstr "Aplikasi ini disetel untuk menerima pembaruan terakhir."

#: ubuntu-mate-welcome:256
#, fuzzy
msgid "was not installed."
msgstr "Pasang ulang"

#: ubuntu-mate-welcome:257 ubuntu-mate-welcome:267 ubuntu-mate-welcome:298
#: ubuntu-mate-welcome:308
msgid "The operation was cancelled."
msgstr ""

#: ubuntu-mate-welcome:259
#, fuzzy
msgid "failed to install"
msgstr "Pasang ulang"

#: ubuntu-mate-welcome:260
msgid "There was a problem installing this application."
msgstr ""

#: ubuntu-mate-welcome:263 ubuntu-mate-welcome:827
msgid "Removed"
msgstr ""

#: ubuntu-mate-welcome:264
#, fuzzy
msgid "The application has been uninstalled."
msgstr "Ukuran partisi root telah diubah."

#: ubuntu-mate-welcome:266
#, fuzzy
msgid "was not removed."
msgstr "Copot pemasangan"

#: ubuntu-mate-welcome:269
msgid "failed to remove"
msgstr ""

#: ubuntu-mate-welcome:270
msgid "A problem is preventing this application from being removed."
msgstr ""

#: ubuntu-mate-welcome:273
#, fuzzy
msgid "Upgraded"
msgstr "Tingkatkan"

#: ubuntu-mate-welcome:274
#, fuzzy
msgid "This application is set to use the latest version."
msgstr "Aplikasi ini disetel untuk menerima pembaruan terakhir."

#: ubuntu-mate-welcome:276
#, fuzzy
msgid "was not upgraded."
msgstr "Tingkatkan"

#: ubuntu-mate-welcome:277
#, fuzzy
msgid "The application will continue to use the stable version."
msgstr "Aplikasi ini disetel untuk menerima pembaruan terakhir."

#: ubuntu-mate-welcome:279
msgid "failed to upgrade"
msgstr ""

#: ubuntu-mate-welcome:280
#, fuzzy
msgid "A problem is preventing this application from being upgraded."
msgstr "Harap tunggu, aplikasi sedang diperbarui..."

#: ubuntu-mate-welcome:294
msgid "applications have been installed"
msgstr ""

#: ubuntu-mate-welcome:295
msgid "Your newly installed applications are ready to use."
msgstr ""

#: ubuntu-mate-welcome:297
msgid "Some applications were not installed."
msgstr ""

#: ubuntu-mate-welcome:300
msgid "Some applications failed to install."
msgstr ""

#: ubuntu-mate-welcome:301 ubuntu-mate-welcome:311
msgid "Software Boutique encountered a problem with one of the applications."
msgstr ""

#: ubuntu-mate-welcome:304
#, fuzzy
msgid "applications have been removed"
msgstr "Ukuran partisi root telah diubah."

#: ubuntu-mate-welcome:305
msgid "These applications are no longer present on your system."
msgstr ""

#: ubuntu-mate-welcome:307
msgid "Some applications were not removed."
msgstr ""

#: ubuntu-mate-welcome:310
msgid "Some applications failed to remove."
msgstr ""

#: ubuntu-mate-welcome:477
msgid "Blu-ray AACS database install succeeded"
msgstr "Pemasangan basis data Blu-ray AACS berhasil"

#: ubuntu-mate-welcome:478
msgid "Successfully installed the Blu-ray AACS database."
msgstr "Berhasil memasang basis data Blu-ray AACS."

#: ubuntu-mate-welcome:478
msgid "Installation of the Blu-ray AACS database was successful."
msgstr "Pemasangan basis data Blu-ray AACS berhasil."

#: ubuntu-mate-welcome:481
msgid "Blu-ray AACS database install failed"
msgstr "Pemasangan basis data Blu-ray AACS gagal"

#: ubuntu-mate-welcome:482
msgid "Failed to install the Blu-ray AACS database."
msgstr "Gagal memasang basis data Blu-ray AACS."

#: ubuntu-mate-welcome:482
msgid "Installation of the Blu-ray AACS database failed."
msgstr "Pemasangan basis data Blu-ray AACS gagal."

#: ubuntu-mate-welcome:767
msgid "Welcome"
msgstr "Selamat Datang"

#: ubuntu-mate-welcome:768
msgid "Software Boutique"
msgstr "Butik Aplikasi"

#: ubuntu-mate-welcome:769
msgid "Close"
msgstr "Tutup"

#: ubuntu-mate-welcome:770
msgid "Cancel"
msgstr ""

#: ubuntu-mate-welcome:773
msgid "Set to retrieve the latest software listings."
msgstr "Setel untuk mengambil daftar aplikasi terbaru."

#: ubuntu-mate-welcome:774
msgid "Retrieve the latest software listings."
msgstr "Mengambil daftar aplikasi terbaru."

#: ubuntu-mate-welcome:775
msgid "Please wait while the application is being updated..."
msgstr "Harap tunggu, aplikasi sedang diperbarui..."

#: ubuntu-mate-welcome:776
msgid "Version:"
msgstr "Versi:"

#: ubuntu-mate-welcome:779
msgid "This application is set to receive the latest updates."
msgstr "Aplikasi ini disetel untuk menerima pembaruan terakhir."

#: ubuntu-mate-welcome:780
msgid "Alternative to:"
msgstr "Alternatif dari:"

#: ubuntu-mate-welcome:781
msgid "Hide"
msgstr "Sembunyikan"

#: ubuntu-mate-welcome:782
msgid "Show"
msgstr "Tampilkan"

#: ubuntu-mate-welcome:783
msgid "Install"
msgstr "Pasang"

#: ubuntu-mate-welcome:784
msgid "Reinstall"
msgstr "Pasang ulang"

#: ubuntu-mate-welcome:785
msgid "Remove"
msgstr "Copot pemasangan"

#: ubuntu-mate-welcome:786
msgid "Upgrade"
msgstr "Tingkatkan"

#: ubuntu-mate-welcome:787
msgid "Launch"
msgstr "Jalankan"

#: ubuntu-mate-welcome:788
msgid "License"
msgstr "Lisensi"

#: ubuntu-mate-welcome:789
msgid "Platform"
msgstr "Platform"

#: ubuntu-mate-welcome:790
msgid "Category"
msgstr "Kategori"

#: ubuntu-mate-welcome:791
msgid "Website"
msgstr "Laman"

#: ubuntu-mate-welcome:792
msgid "Screenshot"
msgstr "Gambar tangkapan"

#: ubuntu-mate-welcome:793
msgid "Source"
msgstr "Sumber"

#: ubuntu-mate-welcome:794
msgid "Canonical Partner Repository"
msgstr "Repositori Mitra Canonical"

#: ubuntu-mate-welcome:795
#, fuzzy
msgid "Ubuntu Multiverse Repository"
msgstr "Repositori Ubuntu"

#: ubuntu-mate-welcome:796
msgid "Ubuntu Repository"
msgstr "Repositori Ubuntu"

#: ubuntu-mate-welcome:797
msgid "Unknown"
msgstr "Tidak dikenal"

#: ubuntu-mate-welcome:798
msgid "Undo Changes"
msgstr ""

#: ubuntu-mate-welcome:801
msgid "Installing..."
msgstr "Memasang..."

#: ubuntu-mate-welcome:802
msgid "Removing..."
msgstr "Mencopot..."

#: ubuntu-mate-welcome:803
msgid "Upgrading..."
msgstr "Meningkatkan..."

#: ubuntu-mate-welcome:806
msgid "Accessories"
msgstr ""

#: ubuntu-mate-welcome:807
msgid "Education"
msgstr "Pendidikan"

#: ubuntu-mate-welcome:808
msgid "Games"
msgstr "Permainan"

#: ubuntu-mate-welcome:809
msgid "Graphics"
msgstr "Grafis"

#: ubuntu-mate-welcome:810
msgid "Internet"
msgstr "Internet"

#: ubuntu-mate-welcome:811
msgid "Office"
msgstr "Perkantoran"

#: ubuntu-mate-welcome:812
msgid "Programming"
msgstr "Pemrograman"

#: ubuntu-mate-welcome:813
msgid "Sound & Video"
msgstr "Suara dan Video"

#: ubuntu-mate-welcome:814
msgid "System Tools"
msgstr "Peralatan Sistem"

#: ubuntu-mate-welcome:815
msgid "Universal Access"
msgstr "Akses Menyeluruh"

#: ubuntu-mate-welcome:816
msgid "Server One-Click Installation"
msgstr ""

#: ubuntu-mate-welcome:817
msgid "Miscellaneous"
msgstr ""

#: ubuntu-mate-welcome:820
msgid "Search"
msgstr ""

#: ubuntu-mate-welcome:821
msgid "Please enter a keyword to begin."
msgstr ""

#: ubuntu-mate-welcome:822
msgid "Please enter at least 3 characters."
msgstr ""

#: ubuntu-mate-welcome:825
msgid "Added"
msgstr ""

#: ubuntu-mate-welcome:826
msgid "Fixed"
msgstr ""

#: ubuntu-mate-welcome:830
msgid "Queued for installation."
msgstr ""

#: ubuntu-mate-welcome:831
msgid "Queued for removal."
msgstr ""

#: ubuntu-mate-welcome:832
msgid "Preparing to remove:"
msgstr ""

#: ubuntu-mate-welcome:833
msgid "Preparing to install:"
msgstr ""

#: ubuntu-mate-welcome:834
#, fuzzy
msgid "Removing:"
msgstr "Mencopot..."

#: ubuntu-mate-welcome:835
#, fuzzy
msgid "Installing:"
msgstr "Memasang..."

#: ubuntu-mate-welcome:836
msgid "Updating cache..."
msgstr ""

#: ubuntu-mate-welcome:837
msgid "Verifying software changes..."
msgstr ""

#: ubuntu-mate-welcome:838
msgid "Successfully Installed"
msgstr ""

#: ubuntu-mate-welcome:839
msgid "Failed to Install"
msgstr ""

#: ubuntu-mate-welcome:840
msgid "Successfully Removed"
msgstr ""

#: ubuntu-mate-welcome:841
msgid "Failed to Remove"
msgstr ""

#: ubuntu-mate-welcome:842
#, fuzzy
msgid "To be installed"
msgstr "Pasang ulang"

#: ubuntu-mate-welcome:843
#, fuzzy
msgid "To be removed"
msgstr "Copot pemasangan"

#: ubuntu-mate-welcome:1051
msgid "Servers"
msgstr "Server"

#: ubuntu-mate-welcome:1085
msgid "Jan"
msgstr "Jan"

#: ubuntu-mate-welcome:1086
msgid "Feb"
msgstr "Feb"

#: ubuntu-mate-welcome:1087
msgid "Mar"
msgstr "Mar"

#: ubuntu-mate-welcome:1088
msgid "Apr"
msgstr "Apr"

#: ubuntu-mate-welcome:1089
msgid "May"
msgstr "Mei"

#: ubuntu-mate-welcome:1090
msgid "Jun"
msgstr "Jun"

#: ubuntu-mate-welcome:1091
msgid "Jul"
msgstr "Jul"

#: ubuntu-mate-welcome:1092
msgid "Aug"
msgstr "Agu"

#: ubuntu-mate-welcome:1093
msgid "Sep"
msgstr "Sep"

#: ubuntu-mate-welcome:1094
msgid "Oct"
msgstr "Okt"

#: ubuntu-mate-welcome:1095
msgid "Nov"
msgstr "Nov"

#: ubuntu-mate-welcome:1096
msgid "Dec"
msgstr "Des"

#: ubuntu-mate-welcome:1616
msgid "MB"
msgstr "MB"

#: ubuntu-mate-welcome:1617
msgid "MiB"
msgstr "MiB"

#: ubuntu-mate-welcome:1618
msgid "GB"
msgstr "GB"

#: ubuntu-mate-welcome:1619
msgid "GiB"
msgstr "GiB"

#: ubuntu-mate-welcome:1627
msgid "Could not gather data."
msgstr "Tidak bisa mengumpulkan data."

#: ubuntu-mate-welcome:1926
msgid "Raspberry Pi Partition Resize"
msgstr "Ubah ukuran Partisi Raspberry Pi"

#: ubuntu-mate-welcome:1943
msgid "Root partition has been resized."
msgstr "Ukuran partisi root telah diubah."

#: ubuntu-mate-welcome:1943
msgid "The filesystem will be enlarged upon the next reboot."
msgstr "Sistem berkas akan diperbesar pada muat ulang berikutnya."

#: ubuntu-mate-welcome:1949 ubuntu-mate-welcome:1951 ubuntu-mate-welcome:1953
#: ubuntu-mate-welcome:1955
msgid "Don't know how to expand."
msgstr "Tidak bisa memperbesar."

#: ubuntu-mate-welcome:1949
msgid "does not exist or is not a symlink."
msgstr "tidak ada atau bukan tautan simbolik."

#: ubuntu-mate-welcome:1951
msgid "is not an SD card."
msgstr "bukan kartu SD."

#: ubuntu-mate-welcome:1953
msgid "Your partition layout is not currently supported by this tool."
msgstr "Skema partisi Anda saat ini belum didukung oleh alat ini."

#: ubuntu-mate-welcome:1955
msgid "is not the last partition."
msgstr "bukan partisi terakhir."

#: ubuntu-mate-welcome:1957
msgid "Failed to run resize script."
msgstr ""

#: ubuntu-mate-welcome:1957
msgid "The returned error code is:"
msgstr ""

#: ubuntu-mate-welcome:2160
msgid "Open Source"
msgstr "Sumber Terbuka"

#: ubuntu-mate-welcome:2162
msgid "Proprietary"
msgstr "Berkepemilikan"

#: ubuntu-mate-welcome:2278
msgid ""
"Sorry, Welcome could not feature any software for this category that is "
"compatible on this system."
msgstr ""
"Maaf, Selamat Datang tidak bisa menampilkan aplikasi untuk kategori ini yang "
"kompatibel di sistem ini."

#: ubuntu-mate-welcome:2490
msgid ""
"An error occurred while launching PROGRAM_NAME. Please consider re-"
"installing the application."
msgstr ""
"Terjadi kesalahan saat menjalankan PROGRAM_NAME. Harap pertimbangkan untuk "
"memasang ulang aplikasi."

#: ubuntu-mate-welcome:2491
msgid "Command:"
msgstr "Perintah:"

#: ubuntu-mate-welcome:2573
msgid "Uses a new or updated source."
msgstr ""

#: ubuntu-mate-welcome:2575
msgid "Now works for various versions of Ubuntu."
msgstr ""

#: ubuntu-mate-welcome:2577
msgid "Now a snappy package."
msgstr ""

#: ubuntu-mate-welcome:2579
msgid "General maintenance."
msgstr ""

#: ubuntu-mate-welcome:2581
msgid "Broken or problematic source."
msgstr ""

#: ubuntu-mate-welcome:2583
msgid "Not yet available for some releases."
msgstr ""

#: ubuntu-mate-welcome:2585
msgid "No longer works for some releases."
msgstr ""

#: ubuntu-mate-welcome:2587
msgid "Not suitable for production machines."
msgstr ""

#: ubuntu-mate-welcome:2589
msgid "Requires further testing."
msgstr ""

#: ubuntu-mate-welcome:2591
msgid "Does not meet our standards to be featured."
msgstr ""

#: ubuntu-mate-welcome:2762
msgid "applications found."
msgstr ""

#: ubuntu-mate-welcome:2766
msgid "proprietary applications are hidden."
msgstr ""

#, fuzzy
#~ msgid "Removing software..."
#~ msgstr "Mencopot..."

#, fuzzy
#~ msgid "Installing software..."
#~ msgstr "Memasang..."

#, fuzzy
#~ msgid "Installation of "
#~ msgstr "Memasang..."

#, fuzzy
#~ msgid "Upgrade of "
#~ msgstr "Tingkatkan"

#~ msgid "Skip"
#~ msgstr "Lewati"
